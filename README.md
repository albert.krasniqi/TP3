# TP3 de programmation Web

## Mise en route

Installation des dépendances :
```
npm install
```

Lancement du serveur :
```
node server.js
```

Le serveur est alors accessible à l’adresse http://localhost:1234

La liste des utilisateurs est accessible à l’adresse http://localhost:1234/users

## Objectif

### Création d’un formulaire d’authentification

* Créer un formulaire HTML permettant à l’utilisateur de s’authentifier (identifiant/mot de passe),
* poster les données d’authentification à l’adresse « http://localhost:1234/login »,
* vérifier qu’on obtient une réponse « TODO ».

### Vérification de l’identifiant et du mot de passe de l’utilisateur

* Modifier le code du serveur traitant les requêtes « `POST` » sur « `/login` »,
* insérer une requête SQL de vérification des données d’authentification,
* retourner un bloc de données au format JSON indiquant l’état de l’authentification :
```
{
	status: (true|false)
}
```

### Génération d’une clé de session

* Modifier le code du serveur traitant les requêtes « `POST` » sur « `/login` »,
* si l’utilisateur a été correctement authentifié, alors créer un enregistrement dans la table « `sessions` » (la valeur du champ « `token` » devra être unique),
* retourner cette clé de session dans le bloc de données JSON précédent :
```
{
	status: (true|false),
	token: <valeur-token>
}
```

### Envoi d’un cookie de session

* Modifier le code du serveur traitant les requêtes « `POST` » sur « `/login` »,
* si l’utilisateur a été correctement authentifié, alors renvoyer la clé de session sous forme d’un cookie.

## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
