var port = 1234;

var express = require('express');
var cookieParser = require('cookie-parser')
var jwt = require('jsonwebtoken');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var randtoken = require('rand-token');
var path = require('path');

var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

app.use('/static', express.static('public'));

app.use(cookieParser());

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});
	
app.get("/sessions", function(req, res, next) {
	// Affichage du contenu de la table "sessions"
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json({ requete : data });
	});
});


var status;

// Déconnexion de l'utilisateur, supression de la ligne dans la table "sessions" + valeur du cookie de la session initialisé à "null"
app.post("/logout", function(req, res, next) {

	var cookie = req.cookies.KeySession;
	// Supression de la session dans la table "sessions"
	db.run('DELETE FROM sessions WHERE token = ?', [cookie]);
	// Suppresion du Cookie
	res.cookie("KeySession", "", { maxAge: 1 });
	res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Redirection de l'utilisateur
app.get("/", function(req, res, next) {

	var cookie = req.cookies.KeySession;

	// On vérifie dans la base de données si l'utilisateur existe dans la table "sessions" grâce au cookie
	db.all('SELECT * FROM sessions WHERE token = ? ;', [cookie], function(err, data) {

		// Si l'utilisateur existe, on le connecte directement et le dirige vers la page d'accueil
		if( data.length > 0 ){
			res.sendFile(path.join(__dirname, 'public', 'logout.html'));

		}else{
			// Sinon on le redirige vers la page de connexion
			res.sendFile(path.join(__dirname, 'public', 'index.html'));
		}

	});

});


app.post("/login", function(req, res, next) {

	// Vérifications des identifiants de l'utilisateur
	db.all('SELECT rowid, ident, password FROM users WHERE ident = ? and password = ? ;', [req.body.pseudo, req.body.pwd], function(err, data) {

			// Si les identifiants sont incorrect l'utilisateur ne se connecte pas, on lui affiche son statut de connexion
			if( data.length <= 0 ){
				status = false;
				res.json({status : status});
			}else{
				// Sinon on initialise un token pour l'utilisateur
				status = true;
				// Génère un token "hashé"
				token = randtoken.generate(16);

			// On vérifie dans la base de données si l'utilisateur existe dans la table "sessions"
			db.all('SELECT * FROM sessions WHERE ident = ? ;', [req.body.pseudo], function(err, data2) {

				if( data2.length <= 0 ){
					// Si l'utilisateur n'existe pas dans la table "sessions", on le crée
					db.run('INSERT INTO sessions(ident, token) VALUES(?,?)', [req.body.pseudo, token]);
				}else{
					// Si l'utilisateur est déjà enregistré dans la table sessions, on le met à jour le token pour cet utilisateur
					db.run('UPDATE sessions SET token = ? WHERE ident = ?;', [token, req.body.pseudo]);
				}

			});

			  var cookie = req.cookies.KeySession;

			  if (cookie === undefined) {
			  	// S'il s'agit de la première connexion de l'utilisateur, on initialise le cookie
			    res.cookie('KeySession', token);
			    console.log('Création du Cookie : ');
			    console.log(cookie);
			  } 
			  else {
			  	// Sinon on le met à jour
			  	res.cookie('KeySession', token);
			    console.log('Cookie : ', cookie);
			  }

		  	// Envoie des données
			//res.json({ status : status, token: token });
			res.sendFile(path.join(__dirname, 'public', 'logout.html'));

		}
	});

});

app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
